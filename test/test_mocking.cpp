#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_version_macros.hpp>
#include <catch2/trompeloeil.hpp>

class IAdder {
public:
    virtual int add(int x, int y) = 0;
};

class MockAdder : public IAdder {
public:
    MAKE_MOCK2(add, int(int, int), override);
};

TEST_CASE("IAdder::add") {
    SECTION("success case") {
        MockAdder adder;

        int x = 0;
        int y = 0;
        REQUIRE_CALL(adder, add(ANY(int), ANY(int)))
            .LR_SIDE_EFFECT(x = _1)
            .LR_SIDE_EFFECT(y = _2)
            .RETURN(_1 + _2)
            .TIMES(1);

        REQUIRE(adder.add(2, 3) == 5);
        REQUIRE(x == 2);
        REQUIRE(y == 3);
    }

    SECTION("fail case") {
        MockAdder adder;

        using trompeloeil::eq;

        REQUIRE_CALL(adder, add(eq(2), ANY(int))).RETURN(_1 + _2).TIMES(1);

        REQUIRE(adder.add(1, 2) == 3);
    }
}
