# Test Catch2 Trompeloeil

Test of using [Catch2](https://github.com/catchorg/Catch2)
and [Trompeloeil](https://github.com/rollbear/trompeloeil) together.

## Example of Output

```console
$ ./build/Debug/bin/test_mocking

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
test_mocking is a Catch v3.0.0-preview.3 host application.
Run with -? for options

-------------------------------------------------------------------------------
IAdder::add
  fail case
-------------------------------------------------------------------------------
../../test/test_mocking.cpp:32
...............................................................................

../../extern/Trompeloeil/include/catch2/trompeloeil.hpp:40: FAILED:
explicitly with message:
  No match for call of add with signature int(int, int) with.
    param  _1 == 1
    param  _2 == 2

Tried adder.add(eq(2), ANY(int)) at ../../test/test_mocking.cpp:37
    Expected  _1 == 2

../../test/test_mocking.cpp:39: FAILED:
  REQUIRE( adder.add(1, 2) == 3 )
due to unexpected exception with message:
  Exception translation was disabled by CATCH_CONFIG_FAST_COMPILE

===============================================================================
test cases: 1 | 1 failed
assertions: 6 | 4 passed | 2 failed
```
